// 1. Create a function that will add two numbers. Display the result ONLY in the console.


x = sumAll(500, 3658);

function sumAll() {
  let sum = 0;
  for (let i = 0; i < arguments.length; i++) {
    sum += arguments[i];
  }
  console.log(sum);
}



// 2. Create a function that will subtract two numbers. Display the result ONLY in the console.


const diff = (a, b) => {
    return Math.abs(a - b);
}

console.log(diff(12, 17));




// 3. Create a function that will display a user's top 3 favorite movie characters. Use a prompt to gather user input. Display the result ONLY in the console.


const topThreeIconicCharacter = ['James Bond', 'Harry Potter', 'Captain Jack Sparrow',]

function iconicCharacter(value, index, array) {
  console.log((index+1) + ". "+ value);
}
console.log("================================")
console.log("TOP THREE MOVIE CHARACTERS: ")
console.log("================================")
topThreeIconicCharacter.forEach(iconicCharacter);







// 4. Create a function that calculates a dog's age based on the ratio: 1 dog year: 7 human years.(Meaning, a one year old dog is considered a seven year old human). Log the result in the console "Your doggo is (result) in human years!".

function calculateDogAge(age) {
    let dogYears = 28*age;
    console.log("Your doggo is " + dogYears + " years old in human years!");
}

calculateDogAge(1);
// calculateDogAge(0.5);
// calculateDogAge(12);




// 5. Create a function that takes 3 subject grades as arguments and return the average of the 3 grades. Use a prompt to get the values. Display the result in the console: "Your average is (result)!"


const grades = {
  fname: 'Yuuna',
  lname: 'Naragdao',
  Math: 92,
  English: 94,
  Computer: 95
}
let averageTotal = 0
let averageDivisor = 0
let average = 0

for (const [key, value] of Object.entries(grades)) {
  // console.log (typeof(value));
  console.log(`${key}: ${value}`);

  if(typeof(value) == 'number'){
    averageTotal += value
    averageDivisor++
  } else {
    // alert('cannot add ' + value + ' because it is not number')
}

}
average = averageTotal/averageDivisor
console.log("Your Average is: " + average + "!");